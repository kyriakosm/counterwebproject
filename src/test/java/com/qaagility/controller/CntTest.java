package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.*;

public class CntTest {
    @Test
    public void testDValidInput() {
        Cnt cnt = new Cnt();
        int result = cnt.d(10, 2);
        assertEquals(5, result);
    }


    @Test
    public void testDMaxValueForZeroDivisor() {
        Cnt cnt = new Cnt();
        int result = cnt.d(-10, 0);
        assertEquals(Integer.MAX_VALUE, result);
    }
}
