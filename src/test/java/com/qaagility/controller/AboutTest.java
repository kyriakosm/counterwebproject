package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.*;

public class AboutTest {
    @Test
    public void testDesc() {
        About about = new About();
        String result = about.desc();
        assertTrue(result.contains("This application"));
        assertTrue(result.contains("was copied from somewhere"));
        assertTrue(result.contains("cannot give the details"));
        assertTrue(result.contains("proper copyright notice"));
        assertTrue(result.contains("Please don't tell the police!"));
    }
}
